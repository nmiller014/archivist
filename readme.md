# Archivist

Archivist is an API that allows you to search for books from an aggregation of multiple API's. This is done by sending requests to multiple API shims, each of which reach out to a specific book searching API (they can even scrape webpages).  

The first release aims to include shim support and searching specifically by ISBN. This is done asynchronously and the first API that returns a successful result is used for the results.

## Future goals
Some goals for future releases include:

* Searching based on other parameters
* Dynamically loading and unloading shims
* Also supporting a complete search mode where *all* results are returned instead of just the first responder.